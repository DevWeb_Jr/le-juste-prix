import math
import functions
import datetime
import time
import json

print("\n#####################################")
print("########## LE JUSTE PRIX ############")
print("#####################################\n")

if __name__ == '__main__':

            # on récupère la date du jour (jour-moi-année, heures:minutes)
            this_moment = datetime.datetime.today().strftime('%d-%m-%Y %H:%M')
            print("\nDate de début de la partie : ", this_moment, "\n")

            # on récupère le début du chrono
            start_time = time.perf_counter()

            # on définit une limite de temps
            limit_time = 5

            datas_from_play = functions.play()

            # on récupère la fin du chrono
            end_time = time.perf_counter()

            # on mémorise la durée de la partie
            interval_time = end_time - start_time

            # on arrondit la valeur
            interval_time_rounded = round(interval_time, 1)

            # on affiche True ou False si le joueur gagne ou non, ainsi que le nombre d'essai
            print("\n", datas_from_play)

            # on récupère le temps de la partie en secondes
            print("\nDurée de la partie : ", str(
                interval_time_rounded), "secondes")

            # on affiche la date formatée


            # présentation des données du joueur sous la forme d'une liste [victoire, essais, temps]

            all_datas = []
            all_datas.extend(datas_from_play)
            all_datas.append(interval_time_rounded)
            if all_datas[0] == True:
                all_datas[0] = "Gagne"
            else:
                all_datas[0] = "Perdu"

            txt_data =(datas_from_play, interval_time_rounded)
            mon_dictionnaire = {
                "Gagne": all_datas[0], "Duree": all_datas[2], "Nbr essais": all_datas[1]}

            print(mon_dictionnaire)

            with open('datatxt.txt', 'a+',) as mon_fichier2:
                print(all_datas, file=mon_fichier2)


            with open('data.json', 'a+',) as mon_fichier:
                json.dump(mon_dictionnaire, mon_fichier)

#   if jeu == 0:
#           with open('data.json', 'r') as mon_fichier:
#                data = json.load(mon_fichier)
#                print(data)



    ###BONUS###
    # #condition de victoire liée au temps
    # time_victory = False
    # if interval_time < limit_time:
    #     time_victory == True
    # else:
    #     time_victory == False
        
    # #on affiche True ou False
    # print(time_victory)
    