import CONST
import time
import datetime
import math
import json


# demander à l'utilisateur de saisir un nombre
# le contraindre à saisir un int()
# vérifier qu'il ne soit pas un float() ni un str()


def start():
    start = str(
        input("\t(APPUYEZ SUR ENTREE)\n"))
    while start != "":
        start = str(
            input("\t(APPUYEZ SUR ENTREE)\n"))


def play(victory=False):
    # récupération du juste prix dans la variable mysterious_number
    mysterious_number = CONST.RIGHT_PRICE
    # récupération d'un nombre de tentatives dans la variable chance
    chance = CONST.ATTEMPTS

    # on assigne à la variable victoire la valeur False
    victory = False

def play(victory=False):
    #récupération du juste prix dans la variable mysterious_number
    mysterious_number = CONST.RIGHT_PRICE
    #récupération d'un nombre de tentatives dans la variable chance
    chance = CONST.ATTEMPTS
    
    #on assigne à la variable victoire la valeur False
    victory = False
    
    #initialisation de variables à 0
    number_int = 0
    trying = 0
    
    #lancement du programme
    start()
    
    #tant que cette variable vaut 0 :
        #on essaie de vérifier si :
    while chance > 0:
        try:
            if 0 <= number_int <= 200:
                print("\t########", mysterious_number, "########\n")
                while chance > 0:
                    number = input("Saisir un nombre entre 1 et 200 : ")
                    number_int = int(number)    
                    if number_int < mysterious_number:
                        print("\n\t + grand !\n")
                        chance -= 1
                        trying += 1
                        if chance > 1:
                            print("\t(encore", chance, "tentatives)\n")
                        else:
                            print("\t(encore", chance, "tentative)\n")
                    elif number_int > mysterious_number:
                        print("\n\t + petit !\n")
                        chance -= 1
                        trying += 1
                        if chance > 1:
                            print("\t(encore", chance, "tentatives)\n")
                        else:
                            print("\t(encore", chance, "tentative)\n")
                    else:
                        print("\n\n\t\tGAGNÉ\n\n\t******* | " +
                              str(mysterious_number) + " | *******")
                        trying += 1
                        if victory == False:
                            victory = True
                        break

                    print("\n\n\t\tPERDU\n\n\t******* | " +
                          str(mysterious_number) + " | *******")

            else:
                print("un nombre ENTRE 0 et 200 s'il vous plait.")

        except ValueError:
            print("ERREUR : vous devez saisir un nombre entier.")
            play()

        return victory, trying


